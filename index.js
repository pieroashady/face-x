const express = require('express');
const cors = require('cors');
const app = express();
const PORT = process.env.PORT || 4000;
const multer = require('multer');
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './controllers/uploads');
  },
  filename: (req, file, cb) => {
    cb(null, `${file.fieldname}-${Date.now()}.jpg`);
  }
});
const upload = multer({ storage: storage });
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');
const path = require('path');
const fs = require('fs');

const RecoController = require('./controllers/RecoController');
const FaceController = require('./controllers/FaceControllers');

app.all('*', function(req, res, next) {
  var origin = req.get('origin');
  res.header('Access-Control-Allow-Origin', origin);
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.use(cors());
// routes
app.get('/forx', (req, res) => res.send('hello worlds'));

app.post('/api/v1/face-x', RecoController.recognition);
app.post('/api/v1/face-y', RecoController.detectFace);
app.post('/api/v1/upload-face', RecoController.uploadImage);

app.post('/facex', FaceController.recognition);
app.post('/facey', FaceController.detectFace);

app.listen(PORT, () => console.log(`app listening on http://localhost:${PORT}`));
