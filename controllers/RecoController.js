require('dotenv').config();
const Parse = require('parse/node');
require('@tensorflow/tfjs-node');
require('@tensorflow/tfjs-core');
const faceapi = require('face-api.js');
const canvas = require('canvas');
const { Canvas, Image, ImageData } = canvas;
const BusBoy = require('busboy');
const path = require('path');
const os = require('os');
const fs = require('fs');
const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');

class RecoController {
  static async getImages(req, res) {
    Parse.initialize(process.env.APP_ID, process.env.JAVASCRIPT_KEY, process.env.MASTER_KEY);
    Parse.serverURL = 'https://parseapi.back4app.com/';

    let Dataset = Parse.Object.extend('Dataset');
    let query = new Parse.Query(Dataset);

    let results = await query.find();
    return res.json(results);
  }

  static uploadImage(req, res) {
    const BusBoy = require('busboy');
    const path = require('path');
    const os = require('os');
    const fs = require('fs');

    const busboy = new BusBoy({ headers: req.headers });

    let imageFileName;
    let imageToBeUploaded = {};
    let labelLength = 0;
    let username = '';

    const countTotalFiles = (subFile) => {
      const { lstatSync, readdirSync } = require('fs');
      const { join } = require('path');

      //const isDirectory = lstatSync('./dataset/Aldi').isDirectory();
      const getDirectories = readdirSync(`./dataset/${subFile}`).map((name) => join(name));

      return getDirectories;
    };

    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
      console.log(
        'File [' + fieldname + ']: filename: ' + filename + ', encoding: ' + encoding + ', mimetype: ' + mimetype
      );

      if (filename === '') {
        return res.status(200).json({
          status: 2,
          message: 'No image found'
        });
      }

      const imageExtension = filename.split('.')[filename.split('.').length - 1];
      imageFileName = filename.split('.')[0];

      let fileRow;

      //imageFileName = `${new Date().getTime()}.${imageExtension}`;
      const filePath = path.join(__dirname, `../dataset/${imageFileName}`);

      if (!fs.existsSync(filePath)) {
        fs.mkdirSync(filePath);
        fileRow = '1.jpg';
      } else {
        let totalFile = countTotalFiles(imageFileName);
        fileRow = `${totalFile.length + 1}.jpg`;
      }

      let fullPath = path.join(filePath, `./${fileRow}`);
      console.log(fullPath);
      imageToBeUploaded = { fullPath, mimetype };
      file.pipe(fs.createWriteStream(fullPath));
      console.log(imageToBeUploaded.filePath);
    });
    busboy.on('field', (fieldname, val) => {
      console.log('Field [' + fieldname + ']: value: ' + val);
      username = val;
    });
    busboy.on('finish', (field, val) => {
      console.log('field ' + field);
      console.log('Done parsing form!');
      return res.status(200).json({
        face_name: imageFileName,
        message: 'Success uploading image'
      });
    });
    //busboy.end(req.rawBody);
    req.pipe(busboy);
  }

  static recognition(req, res) {
    const busboy = new BusBoy({ headers: req.headers });

    let imageFileName;
    let imageToBeUploaded = {};
    let labelLength = 0;

    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
      console.log(
        'File [' + fieldname + ']: filename: ' + filename + ', encoding: ' + encoding + ', mimetype: ' + mimetype
      );

      if (filename === '') {
        return res.status(200).json({
          status: 2,
          message: 'No image found'
        });
      }

      const imageExtension = filename.split('.')[filename.split('.').length - 1];
      imageFileName = `${new Date().getTime()}.${imageExtension}`;
      const filePath = path.join(__dirname, `./uploads/${imageFileName}`);
      imageToBeUploaded = { filePath, mimetype };
      file.pipe(fs.createWriteStream(filePath));
      console.log(imageToBeUploaded.filePath);
      //runRecognition(imageToBeUploaded.filePath);
    });
    busboy.on('finish', function() {
      console.log('Done parsing form!');

      runRecognition(imageToBeUploaded.filePath);
      //res.end();
    });
    //busboy.end(req.rawBody);
    req.pipe(busboy);

    function directoryNames() {
      const { lstatSync, readdirSync } = require('fs');
      const { join } = require('path');

      const isDirectory = lstatSync('./dataset/Aldi').isDirectory();
      const getDirectories = readdirSync('./dataset').map((name) => join(name));

      return getDirectories;
    }

    function countTotalFiles(subFile) {
      const { lstatSync, readdirSync } = require('fs');
      const { join } = require('path');

      const isDirectory = lstatSync('./dataset/Aldi').isDirectory();
      const getDirectories = readdirSync(`./dataset/${subFile}`).map((name) => join(name));

      return getDirectories;
    }

    function runRecognition(file) {
      faceapi.env.monkeyPatch({
        Canvas,
        Image,
        ImageData
      });

      const baseDir = path.resolve(__dirname, './out');
      function saveFile(fileName, buf) {
        if (!fs.existsSync(baseDir)) {
          fs.mkdirSync(baseDir);
        }
        fs.writeFileSync(path.resolve(baseDir, fileName), buf);
      }

      async function run() {
        await faceapi.nets.ssdMobilenetv1.loadFromDisk('./models');
        await faceapi.nets.faceLandmark68Net.loadFromDisk('./models');
        await faceapi.nets.faceRecognitionNet.loadFromDisk('./models');

        const labeledFaceDescriptors = await loadLabeledImages();
        const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6);
        const queryImage = await canvas.loadImage(file);

        faceapi.SsdMobilenetv1();

        const resultsQuery = await faceapi
          .detectAllFaces(queryImage, faceapi.SsdMobilenetv1Options({ maxResults: 100 }))
          .withFaceLandmarks()
          .withFaceDescriptors();

        if (resultsQuery.length === 0) {
          return res.status(200).json({ status: -2, error: 'No face detected' });
        }

        if (!resultsQuery) {
          return res.status(200).json({ status: -2, error: 'No face detected' });
        }
        let bestMatch = {};

        resultsQuery.map((res) => {
          bestMatch = faceMatcher.findBestMatch(res.descriptor);
          console.log(bestMatch);
          return new faceapi.draw.DrawBox(res.detection.box, { label: bestMatch.toString() });
        });

        // const outQuery = faceapi.createCanvasFromMedia(queryImage);
        // queryDrawBoxes.forEach((drawBox) => drawBox.draw(outQuery));
        // const filename = `recognition-${new Date().getTime()}.jpg`;
        // saveFile(filename, outQuery.toBuffer('image/jpeg'));
        console.log('sukses');
        let distance = bestMatch._distance;
        let distancePercentage = (100 - Number(distance) * 33).toFixed(2);
        let x;
        let resultName = bestMatch._label;
        console.log(resultName);
        if (distance > 0.6) {
          console.log('unknown');
          x = 'Unrecognized';
          return res.status(200).json({ status: -1, result: x, message: 'Muka ga dikenal.', totalImages: labelLength });
        }
        if (resultName === undefined) {
          console.log('undefined');
          return res.status(200).json({
            status: 0,
            message: 'Undefined',
            totalImages: labelLength
          });
        }
        x = 'Recognized';
        console.log('dikenal');
        return res.status(200).json({
          status: 1,
          name: resultName,
          distance: `${distancePercentage}%`,
          result: x,
          message: `Muka kamu ${distancePercentage}% mirip ${resultName}`,
          totalImages: labelLength
        });
      }

      run();

      async function loadLabeledImages() {
        const labelAldi = directoryNames();
        labelLength = labelAldi.length;
        console.log(`Scanning ${labelAldi.length} names`);

        // let Dataset = Parse.Object.extend('Dataset');
        // let query = new Parse.Query(Dataset);

        // let imagesFromDb = await query.find();
        // let response = {};
        // imagesFromDb.map((image) => {
        //   response.imageUrl = image.get('imageUrl');
        // });

        // try {
        return Promise.all(
          labelAldi.map(async (label) => {
            let totalFile = countTotalFiles(label);
            let faceDescriptors = [];

            for (let i = 1; i <= totalFile.length; i++) {
              const refImages = await canvas.loadImage(`./dataset/${label}/${i}.jpg`);
              const fullFaceDescription = await faceapi
                .detectSingleFace(refImages)
                .withFaceLandmarks()
                .withFaceDescriptor();

              if (!fullFaceDescription) {
                console.log(`no face for ${label}`);
                return res.status(200).json({ status: -2, message: 'No face detected' });
              }
              faceDescriptors.push(fullFaceDescription.descriptor);
            }

            return new faceapi.LabeledFaceDescriptors(label, faceDescriptors);
          })
        );
      }
    }
  }

  static detectFace(req, res) {
    const BusBoy = require('busboy');
    const path = require('path');
    const os = require('os');
    const fs = require('fs');
    require('@tensorflow/tfjs-node');
    require('@tensorflow/tfjs-core');
    const fetch = require('node-fetch');
    const faceapi = require('face-api.js');
    const canvas = require('canvas');
    const { Canvas, Image, ImageData } = canvas;

    const busboy = new BusBoy({ headers: req.headers });

    let imageFileName;
    let imageToBeUploaded = {};

    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
      console.log(
        'File [' + fieldname + ']: filename: ' + filename + ', encoding: ' + encoding + ', mimetype: ' + mimetype
      );

      const imageExtension = filename.split('.')[filename.split('.').length - 1];
      imageFileName = `${new Date().getTime()}.${imageExtension}`;
      const filePath = path.join(os.tmpdir(), imageFileName);
      imageToBeUploaded = { filePath, mimetype };
      file.pipe(fs.createWriteStream(filePath));
      console.log(imageToBeUploaded.filePath);
      detect(imageToBeUploaded.filePath);

      file.on('data', function(data) {
        console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
      });
      file.on('end', function() {
        console.log('File [' + fieldname + '] Finished');
      });
    });
    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
      console.log('Field [' + fieldname + ']: value: ' + inspect(val));
    });
    busboy.on('finish', function() {
      console.log('finish ' + imageToBeUploaded.filePath);
      console.log('Done parsing form!');
    });
    req.pipe(busboy);

    async function detect(file) {
      faceapi.env.monkeyPatch({
        Canvas,
        Image,
        ImageData
      });

      await faceapi.nets.ssdMobilenetv1.loadFromDisk('./models');
      await faceapi.nets.faceLandmark68Net.loadFromDisk('./models');
      await faceapi.nets.faceRecognitionNet.loadFromDisk('./models');

      faceapi.SsdMobilenetv1();

      const queryImage = await canvas.loadImage(file);
      const resultsQuery = await faceapi.detectSingleFace(queryImage).withFaceLandmarks().withFaceDescriptor();

      if (!resultsQuery) {
        return res.json({ error: 'No face detected' });
      }
      return res.json({ message: 'Sukses' });
    }
  }
}

module.exports = RecoController;
